#[macro_use]
extern crate lazy_static;
extern crate rppal;

use std::thread;
use std::time::Duration;
use std::sync::Mutex;

use rppal::gpio::{Gpio, Mode, Level, Trigger};

const GPIO_DIALING: u8 = 17;
const GPIO_ROTOR: u8 = 18;

lazy_static! {
    static ref GPIO: Mutex<Gpio> = Mutex::new(Gpio::new().unwrap());
    static ref STATE: Mutex<PhoneState> = Mutex::new(PhoneState::new());
}

struct PhoneState {
    dialing: bool,
    pulse_count: u8,
}

impl PhoneState {
    pub fn new() -> PhoneState {
        PhoneState {
            dialing: false,
            pulse_count: 0,
        }
    }

    pub fn start_dialing(&mut self) {
        self.pulse_count = 0;
        self.dialing = true;
    }

    pub fn stop_dialing(&mut self) {
        println!("{}", self.pulse_count);
        self.pulse_count = 0;
        self.dialing = false;
    }

    pub fn is_dialing(&self) -> bool {
        self.dialing
    }

    pub fn pulse(&mut self) {
        self.pulse_count += 1;
    }
}

fn main() {
    {
        let mut gpio = GPIO.lock().unwrap();
        gpio.set_mode(GPIO_DIALING, Mode::Input);
        gpio.set_async_interrupt(GPIO_DIALING, Trigger::Both, dialing_trigger).unwrap();
        gpio.set_mode(GPIO_ROTOR, Mode::Input);
        gpio.set_async_interrupt(GPIO_ROTOR, Trigger::FallingEdge, rotor_pulse_trigger).unwrap();
    }

    thread::park();
}

#[inline]
fn debounce(gpio: &Gpio, pin: u8, min: u8) {
    let mut c = 0;
    let mut last = gpio.read(pin).unwrap();
    while c < min {
        let state = gpio.read(pin).unwrap();
        if state == last {
            c += 1;
        } else {
            last = state;
            c = 0;
        }

        thread::sleep(Duration::from_millis(1));
    }
}

fn dialing_trigger(_level: Level) {
    let gpio = GPIO.lock().unwrap();
    debounce(&gpio, GPIO_DIALING, 8);
    if gpio.read(GPIO_DIALING).unwrap() == Level::High {
        STATE.lock().unwrap().start_dialing();
    } else {
        STATE.lock().unwrap().stop_dialing();
    }
}

fn rotor_pulse_trigger(_level: Level) {
    let gpio = GPIO.lock().unwrap();
    debounce(&gpio, GPIO_DIALING, 56);
    let mut state = STATE.lock().unwrap();
    if gpio.read(GPIO_ROTOR).unwrap() == Level::Low && state.is_dialing() {
        state.pulse()
    }
}
